.. Test Docs documentation master file, created by
   sphinx-quickstart on Fri Jan  8 17:29:44 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Test Docs's documentation!
=====================================

.. toctree::
  
  
  beforesetup
  useapp
  license
  contributing

  
Indices and tables
==================

* :ref:`genindex`
* :ref:`search`